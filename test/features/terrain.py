from lettuce import *
from selenium import webdriver
from constants import *

world.web = URL


@before.all
def say_hello():
    print "Hello there!"
    print "Lettuce will start to run tests right now..."

@after.all
def say_goodbye(total):
    print "Result, %d of %d scenarios passed!" % (
        total.scenarios_passed,
        total.scenarios_ran
    )
    print "Goodbye!"

@before.each_scenario
def setup_browser(scenario):
    world.browser = webdriver.Chrome()

@after.each_scenario
def teardown_browser(scenario):
    world.browser.quit()
