Feature: Predios Feature in PMP


  Scenario: Listar Usuarios
    Given I logged with user "16124970-K" and password "3tidchile1404"
    And I click config menu
    And I select "Usuarios" menu option
    I should see "Usuarios" within 10 seconds


  Scenario: Agregar nuevo Usuario
    Given I logged with user "16124970-K" and password "3tidchile1404"
    And I go to "Usuarios" page
    And I click "Agregar" link
    I should see "Nuevo usuario" within 5 seconds
    And I fill in "id_rut" with "0019234234"
    And I fill in "id_email" with "pruebas@mail.com"
    And I fill in "id_first_name" with "TestUser"
    And I fill in "id_last_name" with "TestUser"
    And I fill in "id_password" with "testpassword"
    And I fill in "id_phone" with "98765432"
    And I select "Monitor" from "id_is_staff"
    And I click "Guardar" button
    I should see "Usuarios" within 5 seconds
