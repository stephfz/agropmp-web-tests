Feature: Proyectos Feature in PMP


  Scenario: Listar Proyectos
    Given I logged with user "16124970-K" and password "3tidchile1404"
    And I click config menu
    And I select "Proyectos" menu option
    I should see "Proyectos" within 10 seconds


  Scenario: Agregar nuevo Proyecto
    Given I logged with user "16124970-K" and password "3tidchile1404"
    And I go to "Proyectos" page
    And I click "Agregar" link
    I should see "Nuevo proyecto" within 5 seconds
    And I fill in "id_name" with "TestProject"
    And I click "Guardar" button
    I should see "Proyectos" within 5 seconds
    I should see "TestProject" listed on table

  Scenario: Eliminar Proyecto
    Given I logged with user "16124970-K" and password "3tidchile1404"
    And I go to "Proyectos" page
    And I click "Agregar" link
    I should see "Nuevo proyecto" within 5 seconds
    And I fill in "id_name" with "TestProjectToRemove"
    And I click "Guardar" button
    I should see "Proyectos" within 5 seconds
    And I click "Eliminar" on "TestProjectToRemove" item row
    I should see "¿Seguro que desea eliminar este item?" within 5 seconds
    And I click "Eliminar" link
    I should not see "TestProjectToRemove" listed on table
