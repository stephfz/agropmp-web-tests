Feature: Predios Feature in PMP


  Scenario: Listar Predios
    Given I logged with user "16124970-K" and password "3tidchile1404"
    And I click config menu
    And I select "Predios" menu option
    I should see "Predios" within 10 seconds


  Scenario: Agregar nuevo Predio
    Given I logged with user "16124970-K" and password "3tidchile1404"
    And I go to "Predios" page
    And I click "Agregar" link
    I should see "Nuevo predio" within 5 seconds
    And I fill in "id_name" with "00_TestPredio"
    And I select "ALGARROBO" from "id_commune"
    And I select "TID" from "id_company"
    And I click "Guardar" button
    I should see "Predios" within 5 seconds
    I should see "00_TestPredio" listed on table


  Scenario: Eliminar predio
    Given I logged with user "16124970-K" and password "3tidchile1404"
    And I go to "Predios" page
    And I click "Agregar" link
    I should see "Nuevo predio" within 5 seconds
    And I fill in "id_name" with "00_TestPredioToRemove"
    And I select "ALGARROBO" from "id_commune"
    And I select "TID" from "id_company"
    And I click "Guardar" button
    I should see "Proyectos" within 5 seconds
    And I click "Eliminar" on "00_TestPredioToRemove" item row
    I should see "¿Seguro que desea eliminar este item?" within 5 seconds
    And I click "Eliminar" link
    I should not see "00_TestPredioToRemove" listed on table
