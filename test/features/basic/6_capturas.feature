Feature: Capturas Feature in PMP

  Scenario: Listar Capturas
    Given I logged with user "16124970-K" and password "3tidchile1404"
    And I click config menu
    And I select "Capturas" menu option
    I should see "Capturas" within 10 seconds


  Scenario: Abrir Captura
    Given I logged with user "16124970-K" and password "3tidchile1404"
    And I go to "Capturas" page
    And I open "Captura" for item at row "2"
    I should see "detail_modal" displayed
