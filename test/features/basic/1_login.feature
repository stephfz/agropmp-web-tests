Feature: Login in PMP


  Scenario: Login in PMP Web with Correct User
    Given I go to web
    Then I should see "Plataforma de Monitoreo de Plagas" within 20 seconds
    And I fill in "id_username" with "16124970-K"
    And I fill in "id_password" with "3tidchile1404"
    And I click "Ingresar" button
    I should see "Por Verificar" within 10 seconds


  Scenario: Login in PMP Web with Correct User, invalid password
    Given I go to web
    Then I should see "Plataforma de Monitoreo de Plagas" within 20 seconds
    And I fill in "id_username" with "16124970-K"
    And I fill in "id_password" with "badpassword"
    And I click "Ingresar" button
    I should see " Por favor introduza Rut y contraseña correctos. Observa que ambos campos pueden ser sensibles a mayúsculas" within 5 seconds
