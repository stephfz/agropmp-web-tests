Feature: Historial de Capturas Feature in PMP



Scenario: Listar Historial de Capturas
  Given I logged with user "16124970-K" and password "3tidchile1404"
  and I click "Historial" link
  I should see "Historial de fotos y capturas" within 10 seconds


  Scenario: Abrir Captura
    Given I logged with user "16124970-K" and password "3tidchile1404"
    And I click "Historial" link
    I should see "Historial de fotos y capturas" within 10 seconds
    And I click table "events-history"  at row number "1"
    I should see history photo enlarged "event-history-modal"


  Scenario: Abrir Captura, verificar captura
    Given I logged with user "16124970-K" and password "3tidchile1404"
    And I click "Historial" link
    I should see "Historial de fotos y capturas" within 10 seconds
    And I click table "events-history"  at row number "1"
    I should see history photo enlarged "event-history-modal"
    And I click "Sí" verification button

  Scenario: Abrir Captura, verificar captura
    Given I logged with user "16124970-K" and password "3tidchile1404"
    And I click "Historial" link
    I should see "Historial de fotos y capturas" within 10 seconds
    And I click table "events-history"  at row number "1"
    I should see history photo enlarged "event-history-modal"
    And I click "No" verification button
