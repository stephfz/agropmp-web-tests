Feature: Trampas Feature in PMP

  Scenario: Listar Trampas
    Given I logged with user "16124970-K" and password "3tidchile1404"
    And I click config menu
    And I select "Trampas" menu option
    I should see "Trampas" within 10 seconds

  Scenario: Agregar nueva Trampa
    Given I logged with user "16124970-K" and password "3tidchile1404"
    And I go to "Trampas" page
    And I click "Agregar" link
    I should see "Nueva trampa" within 5 seconds
    And I fill in "id_trap_id" with "00_TestTrampa"
    And I fill in "id_description" with "TestTrampa Desc"
    And I select "Fundo Pidihuinco" from "id_place"
    And I select "16124970-K" from "id_monitor"
    And I fill in "id_lat" with "-34.6370000000"
    And I fill in "id_lon" with "-71.1530000000"
    And I click "Guardar" button
    I should see "Trampas" within 5 seconds
    I should see "00_TestTrampa" listed on table

  Scenario: Eliminar Trampa
    Given I logged with user "16124970-K" and password "3tidchile1404"
    And I go to "Trampas" page
    And I click "Agregar" link
    I should see "Nueva trampa" within 5 seconds
    And I fill in "id_trap_id" with "00_TestTrampaToRemove"
    And I fill in "id_description" with "TestTrampa Desc"
    And I select "Fundo Pidihuinco" from "id_place"
    And I select "16124970-K" from "id_monitor"
    And I fill in "id_lat" with "-34.6370000000"
    And I fill in "id_lon" with "-71.1530000000"
    And I click "Guardar" button
    I should see "Trampas" within 5 seconds
    And I click "Eliminar" on "00_TestTrampaToRemove" item row
    I should see "¿Seguro que desea eliminar este item?" within 5 seconds
    And I click "Eliminar" link
    I should not see "00_TestTrampaToRemove" listed on table
