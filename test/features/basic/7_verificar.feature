Feature: Verificar Capturas Feature in PMP



Scenario: Listar Capturas
  Given I logged with user "16124970-K" and password "3tidchile1404"
  And I click "Por Verificar" link
  I should see "Fotos por verificar" within 10 seconds

  Scenario: Verificar Captura
    Given I logged with user "16124970-K" and password "3tidchile1404"
    and I click "Por Verificar" link
    I should see "Foto de la trampa:" within 10 seconds
    And I click "Sí" verification button  


Scenario: Verificar Captura con corrección
  Given I logged with user "16124970-K" and password "3tidchile1404"
  and I click "Por Verificar" link
  I should see "Foto de la trampa:" within 10 seconds
  And I click "No" verification button
  I should see correction input "rc-input-number-input"
  And I input "50" on correction input
  And I click "Guardar" verification button
