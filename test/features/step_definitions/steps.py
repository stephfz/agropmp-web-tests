# -*- encoding: utf-8 -*-
import sys  # sys.setdefaultencoding is cancelled by site.py
reload(sys)  # to re-enable sys.setdefaultencoding()
sys.setdefaultencoding('utf-8')

from lettuce import *
from lettuce_webdriver.util import assert_false
from lettuce_webdriver.util import assert_true
from lettuce_webdriver.util import AssertContextManager
from lettuce_webdriver.webdriver import *
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
import time
import os
from time import gmtime, strftime
from utils import *
from selenium.webdriver.support.ui import Select


@step('I go to web')
def goto(step):
    with AssertContextManager(step):
        world.browser.maximize_window()
        world.browser.get(world.web)


@step('I see text "(.*?)"')
def i_see_text(step, text):
    time.sleep(5)
    variable = world.browser.page_source
    assert  text in world.browser.page_source, "Text: <%s> not found on page" % text

@step('I fill in field with id "(.*?)" with "(.*?)"')
def fill_in_textfield_by_class(step, field_name, value):
    with AssertContextManager(step):
        text_field = find_field_by_id(world.browser, field_name)
        assert_false(step, text_field is False,'Can not find a field named "%s"' % field_name)
        text_field.clear()
        text_field.send_keys(value)


@step('I click "(.*?)" button')
def find_button_by_input_text(step, button_text):
    xpath = "//input[@value = '%s']" % button_text
    elem = world.browser.find_element_by_xpath(xpath)
    elem.click()

@step('I click config menu')
def click_config_menu(step):
    config_menu = world.browser.find_element_by_class_name('fa-cog')
    config_menu.click()

@step('I select "(.*?)" menu option')
def select_menu_option(step, menu_option):
    element = world.browser.find_element_by_link_text(menu_option)
    element.click()

@step('I click "(.*?)" link')
def click_link(step, link_name):
    element = world.browser.find_element_by_partial_link_text(link_name)
    element.click()


def get_item_from_table(project_name):
    tableRows = world.browser.find_elements_by_tag_name('tr')
    row_to_return = None
    for row in tableRows:
        cols = row.find_elements(By.TAG_NAME, "td")
        for col in cols:
            if col.text == project_name:
                row_to_return = row
                break
    return row_to_return

@step('I click "(.*?)" on "(.*?)" item row')
def click_option_on_table_item(step, option_name, item_name):
    item_row = get_item_from_table(item_name)
    assert item_row != None, 'Item: "%s" no está listado' % item_name
    if (option_name == "Eliminar"):
        button = item_row.find_element(By.CLASS_NAME, 'btn-danger')
    elif (option_name == "Editar"):
        button = item_row.find_element(By.LINK_TEXT, 'Editar')
    button.click()


@step('I should not see "(.*?)" listed on table')
def item_not_on_table(steps, item_name):
    item_on_table = get_item_from_table(item_name)
    assert item_on_table == None, "El item %s sigue listado en la tabla" % item_name

@step('I should see "(.*?)" listed on table')
def item_not_on_table(steps, item_name):
    item_on_table = get_item_from_table(item_name)
    assert item_on_table != None, "El item %s no está listado en la tabla" % item_name

@step('I select "(.*?)" from "(.*?)"')
def pick_from_select(step, select_id, item_value):
    select = Select(world.browser.find_element(By.ID, select_id))
    select.select_by_value(item_value)

@step('I open "Captura" for item at row "(.*?)"')
def open_capturas(step, item_index):
    time.sleep(5)
    index = int(item_index) - 1
    element = world.browser.find_element(By.XPATH, '//*[@id="captures"]/tbody/tr[%s]/td[7]/a' % index)
    element.click()
    time.sleep(5)

@step('I should see "(.*?)" displayed')
def is_modal_open(step, modal_id):
    js = "return document.getElementById('%s').getAttribute('style')" % modal_id
    display = world.browser.execute_script(js)
    assert 'block' in display, "Modal %s was not displayed" % modal_id

@step('I click "(.*?)" verification button')
def click_verification_button(step, button_text):
    time.sleep(10)
    element = world.browser.find_element(By.XPATH, "//button[@type='button' and contains(., '%s')]" % button_text)
    element.click()
    time.sleep(5)


@step('I should see correction input')
def correction_input_visible(step):
    element =  world.browser.find_element(By.CLASS_NAME, "rc-input-number-input")
    assert element != None , "El input para corrección de número de polillas no fue mostrado"


@step('I input "(.*?)" on correction input')
def input_corrected_number(step, corrected_number):
    element =  world.browser.find_element(By.CLASS_NAME, "rc-input-number-input")
    element.send_keys(corrected_number)
    time.sleep(5)

@step('I click table "(.*?)"  at row number "(.*?)"')
def click_item_at_index_on_table(step, table_id, item_index):
    time.sleep(5)
    element = world.browser.find_element(By.XPATH, '//*[@id="%s"]/div/div/div[1]/div[2]/table/tbody/tr[%s]/td[2]' % (table_id, item_index))
    element.click()
    time.sleep(5)


@step('I should see history photo enlarged')
def is_history_photo_displayed(step):
    modal =  world.browser.find_elements(By.CLASS_NAME, "event-history-modal")
    assert len(modal) > 0, "La Captura del item seleccionado no fue abierto"

@step('I logged with user "(.*?)" and password "(.*?)"')
def is_logged_in(step, email, password):
    step.given('I go to web')
    step.given('Then I should see "Plataforma de Monitoreo de Plagas" within 20 seconds')
    step.given('And I fill in "id_username" with "%s"' % email)
    step.given('And I fill in "id_password" with "%s"' % password)
    step.given('And I click "Ingresar" button')
    step.given('I should see "Por Verificar" within 10 seconds')

@step('I go to "(.*?)" page')
def go_to_page(step, page_name):
    step.given('I click config menu')
    step.given('I select "%s" menu option' % page_name)
    step.given('I should see "%s" within 10 seconds' % page_name)
